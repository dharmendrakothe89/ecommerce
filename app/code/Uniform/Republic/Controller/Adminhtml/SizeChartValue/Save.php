<?php

namespace Uniform\Republic\Controller\Adminhtml\SizeChartValue;

use Magento\Backend\App\Action;

/**
 * Save Data
 */
class Save extends Action {

    /**
     * @param Action\Context $context
     */
    public function __construct(Action\Context $context) {
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        $data = $this->getRequest()->getPostValue();
        $sizeChartId = $this->getRequest()->getParam('sizechart_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            try {
                $id = $this->getRequest()->getParam('value_id');
                $model = $this->_objectManager->create('Uniform\Republic\Model\SizeChartValue')->load($id);
                $model->setValueOne($data['value1']);
                //$model->setValueTwo($data['value2']);
                $model->setSizechartId($sizeChartId);
                $model->save();

                if ($id) {
                    $this->messageManager->addSuccess(__('Size chart value updated successfully.'));
                } else {
                    $this->messageManager->addSuccess(__('Size chart value created successfully.'));
                }
                
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['value_id' => $model->getId(), 'sizechart_id' => $sizeChartId, '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/index', ['sizechart_id' => $sizeChartId]);
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                echo $e->getMessage();
                $this->messageManager->addException($e, __($e->getMessage()));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['sizechart_id' => $sizeChartId, 'value_id' => $id]);
        }
        return $resultRedirect->setPath('*/*/index', ['sizechart_id' => $sizeChartId]);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Uniform_Republic::advisors');
    }
}
