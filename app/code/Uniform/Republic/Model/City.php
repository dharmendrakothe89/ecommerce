<?php

namespace Uniform\Republic\Model;

use Uniform\Republic\Api\Data\CityInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class City extends AbstractModel implements CityInterface, IdentityInterface {

    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'republic_city';

    /**
     * @var string
     */
    protected $_cacheTag = 'republic_city';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'republic_city';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Uniform\Republic\Model\ResourceModel\City');
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::CITY_ID);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return CityInterface
     */
    public function setId($id)
    {
        return $this->setData(self::CITY_ID, $id);
    }

}