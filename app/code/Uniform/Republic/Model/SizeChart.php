<?php

namespace Uniform\Republic\Model;

use Uniform\Republic\Api\Data\SizeChartInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class SizeChart extends AbstractModel implements SizeChartInterface, IdentityInterface {

    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'republic_size_chart';

    /**
     * @var string
     */
    protected $_cacheTag = 'republic_size_chart';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'republic_size_chart';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Uniform\Republic\Model\ResourceModel\SizeChart');
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::SIZE_CHART_ID);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return SizeChartInterface
     */
    public function setId($id)
    {
        return $this->setData(self::SIZE_CHART_ID, $id);
    }

}