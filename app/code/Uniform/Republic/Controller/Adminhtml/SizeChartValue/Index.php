<?php
namespace Uniform\Republic\Controller\Adminhtml\SizeChartValue;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('sizechart_id', '');
        if (empty($id)) {
            $this->messageManager->addError(__('Please select size advisor'));
            /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();

            return $resultRedirect->setPath('uniform_republic/advisor/index');
        }
        $sizeAdvisor = $this->_objectManager->create('Uniform\Republic\Model\SizeChart')->load($id);
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Uniform_Republic::advisors');
        $resultPage->addBreadcrumb(__('Size Chart Values'), __('Size Chart Values - ' . $sizeAdvisor->getName()));
        $resultPage->addBreadcrumb(__('Manage Size Chart Values'), __('Manage Size Chart Values'));
        $resultPage->getConfig()->getTitle()->prepend(__('Size Chart Values - ' . $sizeAdvisor->getName()));

        return $resultPage;
    }

    /**
     * Is the user allowed to view the advisors
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Uniform_Republic::advisors');
    }


}
