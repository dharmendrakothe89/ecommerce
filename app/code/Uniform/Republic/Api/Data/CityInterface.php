<?php

namespace Uniform\Republic\Api\Data;

interface CityInterface {

    /**
     * Constants for keys of data array.
     */
    const CITY_ID = 'city_id';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

     /**
     * Set ID
     *
     * @param int $id
     * @return \Uniform\Republic\Api\Data\CityInterface
     */
    public function setId($id);
}