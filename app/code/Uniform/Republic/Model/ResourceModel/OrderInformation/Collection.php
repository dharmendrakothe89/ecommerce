<?php
namespace Uniform\Republic\Model\ResourceModel\OrderInformation;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Uniform\Republic\Model\ResourceModel\OrderInformation
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     *
     */
    protected function _construct()
    {
        $this->_init('Uniform\Republic\Model\OrderInformation',
            'Uniform\Republic\Model\ResourceModel\OrderInformation');
    }
}
