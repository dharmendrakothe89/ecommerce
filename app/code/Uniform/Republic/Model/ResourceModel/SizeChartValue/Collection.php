<?php
namespace Uniform\Republic\Model\ResourceModel\SizeChartValue;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'value_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Uniform\Republic\Model\SizeChartValue', 'Uniform\Republic\Model\ResourceModel\SizeChartValue');
    }

}
