<?php

namespace Uniform\Republic\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if (version_compare( $context->getVersion(), '1.0.1', '<' )) {
            $setup->getConnection()->dropColumn( $setup->getTable( 'republic_sizechart_to_product' ), 'sizechart_id' );

            $installer->getConnection()->addColumn(
                $installer->getTable( 'republic_sizechart_to_product' ), 'sizechart_id', [
                    'type' => Table::TYPE_INTEGER,
                    'length' => null,
                    'options' => ['nullable' => true],
                    'comment' => 'Size chart Id'
                ]
            );
            $installer->getConnection()->addColumn(
                $installer->getTable( 'republic_sizechart_to_product' ), 'sizechart_image', [
                    'type' => Table::TYPE_TEXT,
                    'length' => 255,
                    'options' => ['nullable' => true],
                    'comment' => 'Advisor image'
                ]
            );
        }

        if (version_compare( $context->getVersion(), '1.0.2', '<' )) {
            if (!$installer->tableExists( 'republic_city' )) {
                $table = $installer->getConnection()->newTable(
                    $installer->getTable( 'republic_city' )
                )
                    ->addColumn(
                        'city_id', Table::TYPE_INTEGER, null, [
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                    ], 'City ID'
                    )
                    ->addColumn(
                        'country_id', Table::TYPE_INTEGER, null, ['nullable' => false], 'Country Id'
                    )
                    ->addColumn(
                        'zone_id', Table::TYPE_INTEGER, null, ['nullable' => true], 'Zone Id'
                    )
                    ->addColumn(
                        'name', Table::TYPE_TEXT, 255, ['nullable' => true], 'Name'
                    )
                    ->setComment( 'City Table' );
                $installer->getConnection()->createTable( $table );
            }
        }

        /*
         * Save order information
         */
        if (version_compare( $context->getVersion(), '1.0.3', '<' )) {
            if (!$installer->tableExists( 'order_information' )) {
                $table = $installer->getConnection()->newTable(
                    $installer->getTable( 'order_information' )
                )
                    ->addColumn(
                        'id', Table::TYPE_INTEGER, null, [
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                    ], 'ID'
                    )
                    ->addColumn(
                        'order_id',
                        Table::TYPE_TEXT,
                        255, ['nullable' => true],
                        'Order Id'
                    )
                    ->addColumn(
                        'order_date',
                        Table::TYPE_TEXT,
                        255, ['nullable' => true],
                        'Order Date'
                    )
                    ->addColumn(
                        'customer_name',
                        Table::TYPE_TEXT,
                        255, ['nullable' => true],
                        'Customer Name'
                    )
                    ->addColumn(
                        'customer_email',
                        Table::TYPE_TEXT,
                        255, ['nullable' => true],
                        'Customer Email'
                    )
                    ->addColumn(
                        'customer_phone',
                        Table::TYPE_TEXT,
                        255, ['nullable' => true],
                        'Customer Phone Number'
                    )
                    ->addColumn(
                        'payment_address_one',
                        Table::TYPE_TEXT,
                        255, ['nullable' => true],
                        'Customer Address One'
                    )
                    ->addColumn(
                        'payment_address_two',
                        Table::TYPE_TEXT,
                        255, ['nullable' => true],
                        'Customer Address Two'
                    )
                    ->addColumn(
                        'payment_postcode',
                        Table::TYPE_TEXT,
                        20, ['nullable' => true],
                        'Payment postcode'
                    )
                    ->addColumn(
                        'order_status',
                        Table::TYPE_TEXT,
                        20, ['nullable' => true],
                        'Order Status'
                    )
                    ->addColumn(
                        'payment_method',
                        Table::TYPE_TEXT,
                        255, ['nullable' => true],
                        'Payment Method'
                    )
                    ->addColumn(
                        'product_name',
                        Table::TYPE_TEXT,
                        255, ['nullable' => true],
                        'Product Name'
                    )
                    ->addColumn(
                        'product_option',
                        Table::TYPE_TEXT,
                        255, ['nullable' => true],
                        'Product Option'
                    )
                    ->addColumn(
                        'product_qty',
                        Table::TYPE_TEXT,
                        255, ['nullable' => true],
                        'Product Quantity'
                    )
                    ->addColumn(
                        'unit_price',
                        Table::TYPE_TEXT,
                        20, ['nullable' => true],
                        'Product Unit Price'
                    )
                    ->addColumn(
                        'sub_total',
                        Table::TYPE_TEXT,
                        20, ['nullable' => true],
                        'Sub Total'
                    )
                    ->addColumn(
                        'shipping_amount',
                        Table::TYPE_TEXT,
                        20, ['nullable' => true],
                        'shipping Amount'
                    )
                    ->addColumn(
                        'order_total',
                        Table::TYPE_TEXT,
                        20, ['nullable' => true],
                        'order Total'
                    )
                    ->setComment( 'Order Information' );
                $installer->getConnection()->createTable( $table );
            }
        }
        $installer->endSetup();
    }
}
