<?php
namespace Uniform\Republic\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Framework\ObjectManagerInterface;

class SizeAdvisor extends AbstractSource {

    /**
     *
     * @var array
     */
    protected $_optionsData;
    
    /**
     *
     * @var \Magento\Framework\ObjectManagerInterface 
     */
    protected $_objectManager;

    public function __construct(ObjectManagerInterface $objectManager) {
        $this->_objectManager = $objectManager;
    }
    /**
     * getAllOptions
     *
     * @return array
     */
    public function getAllOptions() {
        if ($this->_options === null) {
            $this->_options[] = ['value' => '', 'label' => 'Select Size Advisor'];
            $collection = $this->_objectManager->create('Uniform\Republic\Model\ResourceModel\SizeChart\Collection');
            foreach ($collection as $advisor) {
                $this->_options[] =  ['value' => $advisor->getId(), 'label' => $advisor->getName()];
            }
            
            $this->_options['value'] = 4;
        }
        return $this->_options;
    }
}
