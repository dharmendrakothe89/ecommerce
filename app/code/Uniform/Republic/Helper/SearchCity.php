<?php

namespace Uniform\Republic\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\ObjectManagerInterface;

class SearchCity extends AbstractHelper {

    /**
     * The object manager
     * 
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * Constructor
     *
     * @param Context $context
     * @param array $data
     */
    public function __construct(
    Context $context, ObjectManagerInterface $objectManager
    ) {
        $this->_objectManager = $objectManager;
        parent::__construct($context);
    }

    /**
     * Get cities
     * 
     * @return array
     */
    public function getCities() {
        $clientIp = $this->_request->getClientIp();
        $cityData = ['user_city' => 'Pune'];
        if (!empty($clientIp)) {
            $geopluginURL = 'http://www.geoplugin.net/php.gp?ip=' . $clientIp;
            $addrDetailsArr = unserialize(file_get_contents($geopluginURL));
            $city = $addrDetailsArr['geoplugin_city'];

            if (!empty($city)) {
                $citiData['user_city'] = $city;
            }
        }
        
        $cities = [];
        $cityCollection = $this->_objectManager->create('Uniform\Republic\Model\ResourceModel\City\Collection')
                ->setOrder('name','ASC');
        foreach ($cityCollection as $cityModel) {
            $cities[$cityModel->getId()] = $cityModel->getName();
        }
        
        $cityData['cities'] = $cities;
        
        return $cityData;
    }

}
