<?php

namespace Uniform\Republic\Block\Adminhtml\Product;

use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Registry;

class SizeAdvisor extends \Magento\Backend\Block\Template {

    /**
     *
     * @var string
     */
    protected $_template = 'Uniform_Republic::product/size_adv.phtml';

    /**
     *
     * @var ObjectManagerInterface 
     */
    protected $_objectManager;
    
    /**
     *
     * @var Registry 
     */
    protected $_registry;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param ObjectManagerInterface $objectManager
     * @param array $data
     */
    public function __construct(\Magento\Backend\Block\Template\Context $context,
            ObjectManagerInterface $objectManager, 
            Registry $registry,
            array $data = []) {
        $this->_objectManager = $objectManager;
        $this->_registry = $registry;
        parent::__construct($context, $data);
    }

//    public function getAdvisors() {
//        $options[''] = 'Select Advisor';
//        $collection = $this->_objectManager->create('Uniform\Republic\Model\ResourceModel\SizeChart\Collection');
//        foreach ($collection as $advisor) {
//            $options[$advisor->getId()] = $advisor->getName();
//        }
//        
//        return $options;
//    }
    
    /**
     * Get size advisor
     * @return Uniform\Republic\Model\SizeChartProduct
     */
    public function getSizeAdvisor() {
        $product = $this->_registry->registry('current_product');
        return $this->_objectManager->create('Uniform\Republic\Model\SizeChartProduct')->load($product->getId(), 'product_id');
    }

}
