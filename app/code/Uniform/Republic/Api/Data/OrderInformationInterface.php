<?php

namespace Uniform\Republic\Api\Data;

interface OrderInformationInterface {

    /**
     * Constants for keys of data array.
     */
    const ID = 'id';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

     /**
     * Set ID
     *
     * @param int $id
     * @return SizeChartInterface
     */
    public function setId($id);
}
