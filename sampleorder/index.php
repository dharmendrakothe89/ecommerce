<?php
/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
error_reporting(0);
include 'export.php';
?>
<form name="orderexport" action="index.php" method="post">
    <input type="submit" name="ordersubmit" value="Export Order" required="required"/>
</form>
<?php if(isset($_POST['ordersubmit']) && $_POST['ordersubmit']!=''): ?>
	<?php $orderData = getOrderCollection(); ?>
	<?php if(!empty($orderData)) :?>
		<form name="download" action="download.php" method="post">
		<input type="submit" name="orderdownload" value="Download" />
		<input type="hidden" name="order_number" value="downloadorder" />
		</form>
	<?php endif;?>

<!--<table border="1">
    <tr>
        <th>Order Id</th>
        <th>Date Added</th>
        <th>Customer Name</th>
        <th>Customer Email</th>
        <th>Customer Phone</th>
        <th>Payment Address1</th>
        <th>Payment Address2</th>
        <th>Payment postcode</th>
        <th>Order Status</th>
        <th>Payment Method</th>
        <th>Product Name</th>
        <th>Option</th>
        <th>Quantity</th>
        <th>Unit Price</th>
        <th>Sub Total</th>
        <th>Shipping Amount</th>
        <th>Total</th>
    </tr>
    <?php if(!empty($orderData)) :?>
        <?php foreach ($orderData as $data) : ?>
            <tr>
                <td><?= $data['order_id']?></td>
                <td><?= $data['date']?></td>
                <td><?= $data['name']?></td>
                <td><?= $data['email']?></td>
                <td><?= $data['phone']?></td>
                <td><?= $data['address1']?></td>
                <td><?= $data['address2']?></td>
                <td><?= $data['postcode']?></td>
                <td><?= $data['status']?></td>
                <td><?= $data['method']?></td>
                <td><?= $data['product_name']?></td>
                <td><?= $data['option']?></td>
                <td><?= $data['quantity']?></td>
                <td><?= $data['price']?></td>
                <td><?= $data['sub_total']?></td>
                <td><?= $data['shipping_amount']?></td>
                <td><?= $data['total']?></td>
            </tr>
            <?php ?>
        <?php endforeach;?>
    <?php else :?>
        <tr><td colspan="16">No records Found</td></tr>
    <?php endif;?>
</table>-->
<?php endif;?>
