<?php

namespace Uniform\Republic\Block\Adminhtml\SizeChartValue;

use Magento\Backend\Block\Template;

/**
 * Class Export
 * @package Uniform\Republic\Block\Adminhtml\SizeChartValue
 */
class Export extends Template
{
    /**
     *
     * @var string
     */
    protected $_template = 'Uniform_Republic::product/export.phtml';

    /**
     *
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     *
     * @var Registry
     */
    protected $_registry;

    /**
     * Export constructor.
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }
}
