<?php

namespace Uniform\Republic\Api\Data;

interface SizeChartValueInterface {

    /**
     * Constants for keys of data array.
     */
    const VALUE_ID = 'value_id';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

     /**
     * Set ID
     *
     * @param int $id
     * @return \Uniform\Republic\Api\Data\SizeChartValueInterface
     */
    public function setId($id);
}