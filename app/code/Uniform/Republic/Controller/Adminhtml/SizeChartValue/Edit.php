<?php

namespace Uniform\Republic\Controller\Adminhtml\SizeChartValue;

use Magento\Backend\App\Action;

class Edit extends Action {

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Framework\Registry $registry
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed() {
        return $this->_authorization->isAllowed('Uniform_Republic::advisors');
    }

    /**
     * Init actions
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function _initAction() {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Uniform_Republic::advisors');
        $resultPage->addBreadcrumb(__('Size Chart Value'), __('Size Chart Value'));

        return $resultPage;
    }

    /**
     * Edit registration data
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute() {
        $id = $this->getRequest()->getParam('value_id');
        $sizeChartId = $this->getRequest()->getParam('sizechart_id');
        $model = $this->_objectManager->create('Uniform\Republic\Model\SizeChartValue')->load($id);
        $sizeChart = $this->_objectManager->create('Uniform\Republic\Model\SizeChart')->load($sizeChartId);
        
        if ($id) {
            if (!$model->getId()) {
                $this->messageManager->addError(__('This data is no longer exists.'));
                /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('*/*/');
            } else {
                $sizeChartId = $model->getSizechartId();
            }
        }

        if (empty($sizeChartId)) {
            $this->messageManager->addError(__('Please select size advisor.'));
            /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();

            return $resultRedirect->setPath('*/*/');
        }

        $data = $this->_objectManager->get('Magento\Backend\Model\Session')->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }
        $model->setSizechartId($sizeChartId);
        $this->_coreRegistry->register('sizechart_value', $model);

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();

        $resultPage->addBreadcrumb(
                $id ? __('Edit Size Chart Value') : __('New Size Chart Value'), $id ? __('Edit Size Chart Value') : __('New Size Chart Value')
        );

        $resultPage->getConfig()->getTitle()->prepend(__('Size Size Chart Value - ' . $sizeChart->getName()));

        return $resultPage;
    }

}
