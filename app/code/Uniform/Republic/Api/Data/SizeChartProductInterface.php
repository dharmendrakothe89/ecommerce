<?php

namespace Uniform\Republic\Api\Data;

interface SizeChartProductInterface {

    /**
     * Constants for keys of data array.
     */
    const SIZE_CHART_PRODUCT_ID = 'sizechart_to_product_id';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

     /**
     * Set ID
     *
     * @param int $id
     * @return \Uniform\Republic\Api\Data\SizeChartProductInterface
     */
    public function setId($id);
}