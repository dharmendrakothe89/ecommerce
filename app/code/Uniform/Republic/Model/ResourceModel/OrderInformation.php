<?php

namespace Uniform\Republic\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

/**
 * Class OrderInformation
 * @package Uniform\Republic\Model\ResourceModel
 */
class OrderInformation extends AbstractDb
{
    /**
     * OrderInformation constructor.
     * @param Context $context
     * @param null $connectionName
     */
    public function __construct(
        Context $context,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
    }

    /**
     *  Intialization
     */
    protected function _construct() {
        $this->_init('order_information', 'id');
    }
}
