<?php

namespace Uniform\Republic\Controller\Adminhtml\SizeChartValue;

use Magento\Framework\Controller\ResultFactory;

class Delete extends \Magento\Backend\App\Action {

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute() {
        $id = $this->getRequest()->getParam('value_id');
        $model = $this->_objectManager->create('Uniform\Republic\Model\SizeChartValue')->load($id);

        if (!$model->getId()) {
            $this->messageManager->addError(__('This data is no longer exists.'));
            /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();

            return $resultRedirect->setPath('*/advisor/index');
        }

        $sizeChartId = $model->getSizechartId();
        $model->delete();
        $this->messageManager->addSuccess(
                __('A size chart value has been deleted.')
        );

        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('*/*/index', ['sizechart_id' => $sizeChartId]);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed() {
        return $this->_authorization->isAllowed('Uniform_Republic::advisors');
    }

}
