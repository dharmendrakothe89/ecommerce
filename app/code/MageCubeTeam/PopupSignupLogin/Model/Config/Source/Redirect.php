<?php

/**
 * MageCubeTeam
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageCubeTeam.com license that is
 * available through the world-wide-web at this URL:
 * https://www.magecube.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageCubeTeam
 * @package     MageCubeTeam_PopupSignupLogin
 * @copyright   Copyright (c) 2018 MageCubeTeam (http://www.magecube.com/)
 * @license     https://www.magecube.com/LICENSE.txt
 */

namespace MageCubeTeam\PopupSignupLogin\Model\Config\Source;

class Redirect implements \Magento\Framework\Option\ArrayInterface {

    /**
     * @var null | array[]
     */
    protected $_options = null;

    /**
     * @var \Magento\Cms\Model\Page
     */
    protected $cmsPage;

    /**
     * Redirectto constructor.
     *
     * @param \Magento\Cms\Model\Page $cmsPage
     */
    function __construct(\Magento\Cms\Model\Page $cmsPage) {
        $this->cmsPage = $cmsPage;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray() {
        return $this->_getOptions();
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray() {
        $options = [];
        foreach ($this->_getOptions() as $option) {
            $options[$option['value']] = $option['label'];
        }

        return $options;
    }

    protected function _getOptions() {
        if (null === $this->_options) {
            $options = [
                ['value' => 'current', 'label' => __('Stay on the current page')],
                ['value' => 'custom', 'label' => __('Redirect to Custom URL')],
                ['value' => 'dashboard', 'label' => __('Account Dashboard')],
            ];


            $this->_options = $options;
        }

        return $this->_options;
    }

}
