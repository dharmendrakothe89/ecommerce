<?php
function getOrderCollection()
{
    define('DS', DIRECTORY_SEPARATOR);
    require '../app' . DS . 'bootstrap.php';
    $bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $_SERVER);
    $objectManager = $bootstrap->getObjectManager();
    $state = $objectManager->get('Magento\Framework\App\State');
    $state->setAreaCode('frontend');
    //$date = date('Y-m-d');
    $order = $objectManager->create('Magento\Sales\Model\Order')
        ->getCollection()->setOrder('entity_id','DESC')/*->addFieldToFilter('increment_id', array('eq' =>  '000000383'))
        ->getCollection()->addFieldToFilter('created_at', array('like' => '%'. $dateNew.'%'))*/;
    foreach($order as $_order)
    {
        $payment = $_order->getPayment();
        $method = $payment->getMethodInstance();
        $methodTitle = $method->getTitle();
        $billingAddress = $_order->getBillingAddress();
        $shippingAddress = $_order->getShippingAddress();
        $street = $billingAddress->getStreet();
        $orderItems = $_order->getAllItems();
		$config = '';
        foreach($orderItems as $item) {
            if(round($item->getPrice(),0)!=0) {
                $product = $objectManager->get('Magento\Catalog\Model\Product')->load($item->getId());
				$schoolName = '';
				if (null !== $product->getCustomAttribute('school_name')) {
					$schoolName=  $product->getAttributeText('school_name');
				}

				$configurableOption = $item->getProductOptions();
				if (isset($configurableOption['attributes_info'])) {
					$config = '';
					foreach ($configurableOption['attributes_info'] as $info)
					{	
						$myoptionTitle = $info['label'];
				        $myoptionValue = $info['value'];
						$config .= $myoptionTitle.':'.$myoptionValue. "||";	
					}
				}
				//echo $config;

				
                $orderData[$item->getId()]['order_id'] = '#'.$_order->getData('increment_id');
                $orderData[$item->getId()]['date'] = $_order->getData('created_at');
                $orderData[$item->getId()]['name'] = $billingAddress->getFirstName().' '.$billingAddress->getLastName(); 
                $orderData[$item->getId()]['email'] = $_order->getData('customer_email');
                $orderData[$item->getId()]['phone'] = $billingAddress->getTelephone();
                $orderData[$item->getId()]['address1'] = isset($street[0]) ? $street[0]: '';
                $orderData[$item->getId()]['address2'] = isset($street[1]) ? $street[1]: '';
                $orderData[$item->getId()]['postcode'] = $billingAddress->getPostCode();
                $orderData[$item->getId()]['status'] = $_order->getData('status');
                $orderData[$item->getId()]['method'] = $methodTitle;
                $orderData[$item->getId()]['product_name'] = $item->getName();
                //$orderData[$item->getId()]['model'] = $schoolName;
				$orderData[$item->getId()]['option'] = rtrim($config,'||');;
                $orderData[$item->getId()]['quantity'] = round($item->getQtyOrdered(),0);
                $orderData[$item->getId()]['price'] = 'Rs'.round($item->getPrice(),0);
                $orderData[$item->getId()]['sub_total'] = 'Rs'.round($item->getRowTotal(),0);
                $orderData[$item->getId()]['shipping_amount'] = 'Rs'.round($_order->getBaseShippingAmount(),0);
                $orderData[$item->getId()]['total'] = 'Rs'.round($_order->getGrandTotal(),0);
            }
        }
    }
	/* echo "<pre>";
	print_r($orderData);exit;*/
    return $orderData;

}
?>