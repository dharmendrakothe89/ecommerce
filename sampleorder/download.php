<?php
include 'export.php';
if (isset($_POST['orderdownload'] ) && $_POST['order_number'] == 'downloadorder'):
    //$orderNumber = $_POST['order_number'];
    $orderData = getOrderCollection();

    $delimiter = ",";
    $filename = "order_export_" . date('Y-m-d') . ".csv";

//create a file pointer
    $f = fopen('php://memory', 'w');

//set column headers
    $fields = array('Order ID','Date Added','Customer Name','Customer Email','Customer Phone no','Payment Address1','Payment Address2',
        'Payment postcode',	'Order status',	'Payment Method','Product Name','Option','Quantity','Unit Price','Sub Total','Shipping Amount','Total');
    fputcsv($f, $fields, $delimiter);

    foreach ($orderData as $data) :
        $lineData = array(
            $data['order_id'],
            $data['date'],
            $data['name'],
            $data['email'],
            $data['phone'],
            $data['address1'],
            $data['address2'],
            $data['postcode'],
            $data['status'],
            $data['method'],
            $data['product_name'],
            //$data['model'],
            $data['option'],
            $data['quantity'],
            $data['price'],
            $data['sub_total'],
            $data['shipping_amount'],
            $data['total']
        );

        fputcsv( $f, $lineData, $delimiter );
    endforeach;
//move back to beginning of file
    fseek( $f, 0 );

//set headers to download file rather than displayed
    header( 'Content-Type: text/csv' );
    header( 'Content-Disposition: attachment; filename="' . $filename . '";' );

//output all remaining data on a file pointer
    fpassthru( $f );
endif;