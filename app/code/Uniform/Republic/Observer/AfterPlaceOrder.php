<?php
declare(strict_types=1);

namespace Uniform\Republic\Observer;

use Exception;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order;

/**
 * Class AfterPlaceOrder
 * @package Uniform\Republic\Observer
 */
class AfterPlaceOrder implements ObserverInterface
{
    /**
     * @var Order
     */
    private $order;
    /**
     * @var Product
     */
    private $product;
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * AfterPlaceOrder constructor.
     * @param Order $order
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        Order $order,
        ResourceConnection $resourceConnection
    ) {
        $this->order = $order;
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $orderIds = $observer->getEvent()->getOrderIds();
        try {
            $order = $this->getOrderByIncrementId($orderIds);
            $payment = $order->getPayment();
            $method = $payment->getMethodInstance();
            $methodTitle = $method->getTitle();
            $billingAddress = $order->getBillingAddress();
            $street = $billingAddress->getStreet();
            $orderItems = $order->getAllItems();
            $config = '';
            foreach($orderItems as $item) {
                if(round($item->getPrice(),0)!=0) {
                    $configurableOption = $item->getProductOptions();
                    if (isset($configurableOption['attributes_info'])) {
                        $config = '';
                        foreach ($configurableOption['attributes_info'] as $info)
                        {
                            $myoptionTitle = $info['label'];
                            $myoptionValue = $info['value'];
                            $config .= $myoptionTitle.':'.$myoptionValue. "||";
                        }
                    }
                    $order_id = '#'.$order->getData('increment_id');
                    $date = $order->getData('created_at');
                    $name = $billingAddress->getFirstName().' '.$billingAddress->getLastName();
                    $email = $order->getData('customer_email');
                    $phone = $billingAddress->getTelephone();
                    $address1 = isset($street[0]) ? $street[0]: '';
                    $address2 = isset($street[1]) ? $street[1]: '';
                    $postcode = $billingAddress->getPostCode();
                    $status= $order->getData('status');
                    $method = $methodTitle;
                    $product_name = $item->getName();
                    $option = rtrim($config,'||');;
                    $quantity = round($item->getQtyOrdered(),0);
                    $price = 'Rs'.round($item->getPrice(),0);
                    $sub_total = 'Rs'.round($item->getRowTotal(),0);
                    $shipping_amount = 'Rs'.round($order->getBaseShippingAmount(),0);
                    $total = 'Rs'.round($order->getGrandTotal(),0);
                    $connection= $this->resourceConnection->getConnection();
                    $tableName =  $this->resourceConnection->getTableName('order_information');
                    $sql = "INSERT INTO " . $tableName . "(id,order_id ,order_date,customer_name,customer_email,customer_phone,payment_address_one,payment_address_two,
                                                            payment_postcode,order_status,payment_method,product_name,product_option,product_qty,unit_price,sub_total,
                                                            shipping_amount,order_total) VALUES (null, '".$order_id."','".$date."','".$name."','".$email."','".$phone."',
                                                            '".$address1."','".$address2."','".$postcode."','".$status."',
                                                            '".$method."','".$product_name."','".$option."','".$quantity."','".$price."','".$sub_total."','".$shipping_amount."','".$total."')";
                    $connection->query($sql);
                }
            }
        } catch (Exception $e) {
            throwException($e->getMessage());
        }
    }

    /**
     * @param $incrementId
     * @return mixed
     */
    public function getOrderByIncrementId($incrementId)
    {
        $objectManager = ObjectManager::getInstance();
        $order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($incrementId);
        return $order;
    }
}
