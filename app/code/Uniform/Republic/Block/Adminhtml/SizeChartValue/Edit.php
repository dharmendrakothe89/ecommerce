<?php
namespace Uniform\Republic\Block\Adminhtml\SizeChartValue;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * Initialize blog post edit block
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'value_id';
        $this->_blockGroup = 'Uniform_Republic';
        $this->_controller = 'adminhtml_sizeChartValue';

        parent::_construct();

        $this->buttonList->remove('back');
        if ($this->_isAllowedAction('Uniform_Republic::advisors')) {
            $this->buttonList->update('save', 'label', __('Save Size Chart Value'));
            $this->buttonList->remove('saveandcontinue');
        } else {
            $this->buttonList->remove('save');
        }
    }

    /**
     * Retrieve text for header element depending on loaded post
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        if ($this->_coreRegistry->registry('advisor_data')->getId()) {
            return __("Edit Size Chart Value '%1'", $this->escapeHtml($this->_coreRegistry->registry('sizechart_value')->getName()));
        } else {
            return __('New Size Chart Value');
        }
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    /**
     * Getter of url for "Save and Continue" button
     * tab_id will be replaced by desired by JS later
     *
     * @return string
     */
    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('*/*/save', ['_current' => true, 'back' => 'edit', 'active_tab' => '']);
    }
}