<?php

namespace Uniform\Republic\Controller\Adminhtml\Advisor;

use Magento\Backend\App\Action;

/**
 * Save Data
 */
class Save extends Action {

    /**
     * @param Action\Context $context
     */
    public function __construct(Action\Context $context) {
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            try {
                $id = $this->getRequest()->getParam('sizechart_id');
                $model = $this->_objectManager->create('Uniform\Republic\Model\SizeChart')->load($id);
                $model->setName($data['name']);
                //$model->setOptionName($data['option_name']);
                $model->save();

                if ($id) {
                    $this->messageManager->addSuccess(__('Advisor updated successfully.'));
                } else {
                    $this->messageManager->addSuccess(__('Advisor created successfully.'));
                }
                
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                echo $e->getMessage();
                $this->messageManager->addException($e, __($e->getMessage()));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Uniform_Republic::advisors');
    }
}
