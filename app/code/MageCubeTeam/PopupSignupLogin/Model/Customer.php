<?php

/**
 * MageCubeTeam
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageCubeTeam.com license that is
 * available through the world-wide-web at this URL:
 * https://www.magecube.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageCubeTeam
 * @package     MageCubeTeam_PopupSignupLogin
 * @copyright   Copyright (c) 2018 MageCubeTeam (http://www.magecube.com/)
 * @license     https://www.magecube.com/LICENSE.txt
 */

namespace MageCubeTeam\PopupSignupLogin\Model;

class Customer extends \Magento\Framework\Model\AbstractModel {

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;

    public function __construct(
    \Magento\Framework\Model\Context $context, \Magento\Framework\Registry $registry, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Customer\Model\CustomerFactory $customerFactory
    ) {
        $this->storeManager = $storeManager;
        $this->customerFactory = $customerFactory->create();
        parent::__construct($context, $registry);
    }

    public function getWebsiteId() {
        return $this->storeManager->getWebsite()->getWebsiteId();
    }

    public function userExists($email) {
        $customer = $this->customerFactory->setWebsiteId($this->getWebsiteId());
        if ($this->customerFactory->loadByEmail($email)->getId()) {
            return true;
        } else {
            return false;
        }
    }

}
