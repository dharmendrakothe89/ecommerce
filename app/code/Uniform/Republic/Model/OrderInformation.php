<?php

namespace Uniform\Republic\Model;

use Uniform\Republic\Api\Data\OrderInformationInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class OrderInformation
 * @package Uniform\Republic\Model
 */
class OrderInformation extends AbstractModel implements OrderInformationInterface, IdentityInterface
{

    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'order_information';

    /**
     * @var string
     */
    protected $_cacheTag = 'order_information';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'order_information';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Uniform\Republic\Model\ResourceModel\OrderInformation');
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return OrderInformation
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }
}
