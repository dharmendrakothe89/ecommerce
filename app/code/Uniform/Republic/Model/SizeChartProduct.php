<?php

namespace Uniform\Republic\Model;

use Uniform\Republic\Api\Data\SizeChartProductInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class SizeChartProduct extends AbstractModel implements SizeChartProductInterface, IdentityInterface {

    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'republic_size_chart_product';

    /**
     * @var string
     */
    protected $_cacheTag = 'republic_size_chart_product';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'republic_size_chart_product';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Uniform\Republic\Model\ResourceModel\SizeChartProduct');
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::SIZE_CHART_PRODUCT_ID);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return SizeChartProductInterface
     */
    public function setId($id)
    {
        return $this->setData(self::SIZE_CHART_PRODUCT_ID, $id);
    }

}