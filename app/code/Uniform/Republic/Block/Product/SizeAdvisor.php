<?php

namespace Uniform\Republic\Block\Product;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\ConfigurableProduct\Api\LinkManagementInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\View\Element\Template;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Registry;

/**
 * Class SizeAdvisor
 * @package Uniform\Republic\Block\Product
 */
class SizeAdvisor extends Template
{

    /**
     * The object manager
     *
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $_coreRegistry = null;
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;
    /**
     * @var LinkManagementInterface
     */
    private $linkManagement;
    /**
     * @var SearchCriteriaBuilder
     */
    private $builder;
    /**
     * @var StockRegistryInterface
     */
    private $stockRegistry;

    /**
     * Constructor
     *
     * @param Template\Context $context
     * @param ObjectManagerInterface $objectManager
     * @param Registry $registry
     * @param ProductRepositoryInterface $productRepository
     * @param LinkManagementInterface $linkManagement
     * @param SearchCriteriaBuilder $builder
     * @param StockRegistryInterface $stockRegistry
     */
    public function __construct(
        Template\Context $context,
        ObjectManagerInterface $objectManager,
        Registry $registry,
        ProductRepositoryInterface $productRepository,
        LinkManagementInterface $linkManagement,
        SearchCriteriaBuilder $builder,
        StockRegistryInterface $stockRegistry,
        array $data = []
    ) {
        $this->_objectManager = $objectManager;
        $this->_coreRegistry = $registry;
        $this->productRepository = $productRepository;
        $this->linkManagement = $linkManagement;
        $this->builder = $builder;
        $this->stockRegistry = $stockRegistry;
        parent::__construct($context, $data);
    }

    /**
     * Get size chart product
     *
     * @return \Uniform\Republic\Model\SizeChartProduct
     */
    public function getSizeChartProduct() {
        $product = $this->getProduct();
        return $this->_objectManager->create('Uniform\Republic\Model\SizeChartProduct')
            ->load($product->getId(), 'product_id');
    }

    /**
     * Get size chart advisor
     *
     * @return \Uniform\Republic\Model\SizeChart
     */
    public function getSizeAdvisor($id) {
        return $this->_objectManager->create('Uniform\Republic\Model\SizeChart')->load($id);
    }

    /**
     * Get size advisor value
     *
     * @return \Uniform\Republic\Model\SizeChartValue
     */
    public function getSizeAdvisorValues($sizeChartId) {
        $valueCollection = $this->_objectManager->create('Uniform\Republic\Model\ResourceModel\SizeChartValue\Collection')
                ->addFieldToFilter('sizechart_id',array('eq' => $sizeChartId));

        $values = [];
        if ($valueCollection->count() > 0) {
            foreach ($valueCollection as $val) {
                $values[$val->getValueOne()] = $val->getValueTwo();
            }
        }

        return json_encode($values);
    }

    /**
     * Retrieve product
     *
     * @return \Magento\Catalog\Model\Product
     */
    public function getProduct() {
        $product = $this->_getData('product');
        if (!$product) {
            $product = $this->_coreRegistry->registry('product');
        }
        return $product;
    }

    /**
     * Get media url
     *
     * @return string
     */
    public function getMediaUrl() {
        $storeManager = $this->_objectManager->create('\Magento\Store\Model\StoreManagerInterface');
        return $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

    public function getAttributeDetail($product)
    {
        return $product->getResource()->getAttribute('shoulder_inches')
            ->getFrontend();
    }

    /**
     * @return mixed
     */
    public function getChilds()
    {
        $parentId = $this->_coreRegistry->registry('current_product')->getId();
        $product = $this->productRepository->getById($parentId);
        $productTypeInstance = $product->getTypeInstance();
        $usedProducts = $productTypeInstance->getUsedProducts($product);
        foreach ($usedProducts  as $child) {
            $size = $product->getResource()->getAttribute('size')
                ->getFrontend()->getValue($child);
            $id[$size] = [
                'check_size' => $this->getAttributeDetail($product)->getValue($child),
                'attribute_value'=> $size,
                'attribute_label' => $this->getAttributeDetail($product)->getLabel($child),
                $size => $this->getAttributeDetail($product)->getValue($child)
            ];
        }
       return json_encode($id);
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    function getAssociatedProduct()
    {
        $parentId = $this->_coreRegistry->registry('current_product')->getId();
        $product = $this->productRepository->getById($parentId);
        $productTypeInstance = $product->getTypeInstance();
        if($product->getTypeId() == Configurable::TYPE_CODE){
            $usedProducts = $productTypeInstance->getUsedProducts($product);
        }
        return $usedProducts;
    }

    /**
     * @return StockRegistryInterface
     */
    function getStockData()
    {
        return $this->stockRegistry;
    }
}
