<?php

namespace Uniform\Republic\Model;

use Uniform\Republic\Api\Data\SizeChartValueInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class SizeChartValue extends AbstractModel implements SizeChartValueInterface, IdentityInterface {

    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'republic_size_chart_value';

    /**
     * @var string
     */
    protected $_cacheTag = 'republic_size_chart_value';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'republic_size_chart_value';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Uniform\Republic\Model\ResourceModel\SizeChartValue');
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::VALUE_ID);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return SizeChartValueInterface
     */
    public function setId($id)
    {
        return $this->setData(self::VALUE_ID, $id);
    }
    
    /**
     * Get value one
     *
     * @return mixed
     */
    public function getValueOne()
    {
        return $this->getData('value1');
    }

    /**
     * Set value one
     *
     * @param mixed
     * @return SizeChartValueInterface
     */
    public function setValueOne($value1)
    {
        return $this->setData('value1', $value1);
    }

    /**
     * Get value Two
     *
     * @return mixed
     */
    public function getValueTwo()
    {
        return $this->getData('value2');
    }

    /**
     * Set value two
     *
     * @param mixed
     * @return SizeChartValueInterface
     */
    public function setValueTwo($value2)
    {
        return $this->setData('value2', $value2);
    }

}