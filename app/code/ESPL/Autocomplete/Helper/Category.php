<?php

namespace ESPL\Autocomplete\Helper;

use Magento\Framework\App\Helper\Context;

class Category extends \Magento\Framework\App\Helper\AbstractHelper {

    private $storeManager;
    private $Categorycollcetion;
    private $categorymodel;

    public function __construct(
    Context $context, \Magento\Catalog\Model\CategoryFactory $Categorycollcetion, \Magento\Store\Model\StoreManagerInterface $store, \Magento\Catalog\Model\Category $categorymodel
    ) {
        $this->storeManager = $store;
        $this->Categorycollcetion = $Categorycollcetion;
        $this->categorymodel = $categorymodel;
        parent::__construct($context);
    }

    public function getJson() {
        $categoriescollcetion = $this->categorymodel->getCollection()->addAttributeToSelect('name')
                        ->addAttributeToSelect('default_sort_by')->addAttributeToSort('position', 'desc')
                        ->addAttributeToFilter('is_active', [
                            'in' => [
                                1
                            ]
                        ])->addAttributeToFilter('level', [
                    'in' => [
                        2
                    ]
                ])->load()->toArray();

        $categoryarray = [];
        foreach ($categoriescollcetion as $categoryId => $category) {
            if ($category ['parent_id'] == $this->storeManager->getStore()->getRootCategoryId()) {
                $categoryarray[$categoryId] = [
                    'name' => $category ['name'],
                    'type_id' => '',
                    'sku' => '',
                    'image' => $this->getcategoryImages($categoryId),
                    'url_path' => $this->getcategoryUrl($categoryId),
                    'min_price' => '',
                    'price' => '',
                    'final_price' => '',
                    'max_price' => '',
                    'type' => 'category'
                ];
            } else {
                $categoryarray[$categoryId] = [
                    'name' => $this->getcategoryName($category ['parent_id']) . ' > ' . $category ['name'],
                    'type_id' => '',
                    'sku' => '',
                    'image' => $this->getcategoryImages($categoryId),
                    'url_path' => $this->getcategoryUrl($categoryId),
                    'min_price' => '',
                    'price' => '',
                    'final_price' => '',
                    'max_price' => '',
                    'type' => 'category'
                ];
            }
        }

        $finalcategoryarray = [];
        $count = 0;
        foreach ($categoryarray as $key => $value) {
            $finalcategoryarray[$count] = $categoryarray[$key];
            $count++;
        }

        $data = json_encode($finalcategoryarray);
        
        return $data;
    }

    private function loadcategory($_category) {
        return $this->Categorycollcetion->create()->load($_category);
    }

    private function getcategoryImages($entity_id) {
        $category = $this->loadcategory($entity_id);
        return $category->getImageUrl();
    }

    private function getcategoryUrl($entity_id) {
        $category = $this->loadcategory($entity_id);
        return $category->getUrl();
    }

    private function getcategoryName($entity_id) {
        $category = $this->loadcategory($entity_id);
        return $category->getName();
    }

}
