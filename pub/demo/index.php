<?php

echo __LINE__;die();
/**
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
define('DS', DIRECTORY_SEPARATOR);
require '../../app' . DS . 'bootstrap.php';

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Bootstrap;


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('memory_limit', '5G');
error_reporting(E_ALL);

/*$filename = "toy_csv.csv";
$fp = fopen('php://output', 'w');

header('Content-type: application/csv');
header('Content-Disposition: attachment; filename='.$filename);
fputcsv($fp, $header);*/



$bootstrap = Bootstrap::create(BP, $_SERVER);

$objectManager = $bootstrap->getObjectManager();
//Store id of exported products, This is useful when we have multiple stores.
$store_id = 1;

$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
$orderData =array();
$order = array();
$orderData = getOrderCollection();

function getOrderCollection()
{
    $bootstrap = Bootstrap::create(BP, $_SERVER);
    $objectManager = $bootstrap->getObjectManager();
    $state = $objectManager->get('Magento\Framework\App\State');
    $state->setAreaCode('frontend');
    $order = $objectManager->create('Magento\Sales\Model\Order')
        ->getCollection();
    //echo '<pre>';print_r($order->getData());die;
    foreach($order as $_order)
    {
        $orderData = array();
        $payment = $_order->getPayment();
        $method = $payment->getMethodInstance();
        $methodTitle = $method->getTitle();
        $billingAddress = $_order->getBillingAddress();
        $shippingAddress = $_order->getShippingAddress();
        $street = $billingAddress->getStreet();
        $orderItems = $_order->getAllItems();
        foreach($orderItems as $item) {
            $product = $objectManager->get('Magento\Catalog\Model\Product')->load($item->getId());
            $orderData[$item->getId()]['order_id'] = $_order->getData('increment_id');
            $orderData[$item->getId()]['date'] = $_order->getData('created_at');
            $orderData[$item->getId()]['name'] = $_order->getData('customer_firstname').'' .$_order->getData('customer_lastname');
            $orderData[$item->getId()]['email'] = $_order->getData('customer_email');
            $orderData[$item->getId()]['phone'] = $billingAddress->getTelephone();
            $orderData[$item->getId()]['address1'] = isset($street[0]) ? $street[0]: '';
            $orderData[$item->getId()]['address2'] = isset($street[1]) ? $street[1]: '';
            $orderData[$item->getId()]['postcode'] = $billingAddress->getPostCode();
            $orderData[$item->getId()]['status'] = $_order->getData('status');
            $orderData[$item->getId()]['method'] = $methodTitle;

            $orderData[$item->getId()]['product_name'] = $item->getName();
            //$orderData[$item->getId()]['Option'] = $item->getName();
            $orderData[$item->getId()]['model'] = $product->getAttributeText('school_name');
            $orderData[$item->getId()]['quantity'] = round($item->getQtyOrdered(),0);
            $orderData[$item->getId()]['price'] = 'Rs'.round($item->getPrice(),0);
            $orderData[$item->getId()]['sub_total'] = 'Rs'.round($item->getRowTotal(),0);
            $orderData[$item->getId()]['shipping_amount'] = 'Rs'.round($_order->getBaseShippingAmount(),0);
            $orderData[$item->getId()]['total'] = 'Rs'.round($_order->getTotalPaid(),0);
        }
    }
    return $orderData;

}

?>
<table border="1">
    <tr>
    <th>Order Id</th>
    <th>Date Added</th>
    <th>Customer Name</th>
    <th>Customer Email</th>
    <th>Payment Address1</th>
    <th>Payment Address2</th>
    <th>Order Status</th>
    <th>Product Name</th>
    <th>Model</th>
    <th>Quantity</th>
    <th>Model</th>
    <th>Unit Price</th>
    <th>Sub Total</th>
    <th>Shipping Amount</th>
    <th>Total</th>
    </tr>
    <?php if(!empty($orderData)) :?>
        <?php foreach ($orderData as $data) : ?>
        <tr>
            <td><?= $data['order_id']?></td>
            <td><?= $data['date']?></td>
            <td><?= $data['name']?></td>
            <td><?= $data['email']?></td>
            <td><?= $data['phone']?></td>
            <td><?= $data['address1']?></td>
            <td><?= $data['address2']?></td>
            <td><?= $data['status']?></td>
            <td><?= $data['product_name']?></td>
            <td><?= $data['model']?></td>
            <td><?= $data['quantity']?></td>
            <td><?= $data['price']?></td>
            <td><?= $data['sub_total']?></td>
            <td><?= $data['shipping_amount']?></td>
            <td><?= $data['total']?></td>
        </tr>
        <?php ?>
        <?php endforeach;?>
    <?php else :?>

    <?php endif;?>
</table>
