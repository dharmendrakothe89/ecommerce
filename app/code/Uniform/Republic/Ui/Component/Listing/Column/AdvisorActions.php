<?php
namespace Uniform\Republic\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;

class AdvisorActions extends Column
{
    /** Url path */
    const ADVISOR_URL_PATH_EDIT = 'uniform_republic/advisor/edit';
    
    /** Url path */
    const ADVISOR_URL_PATH_DELETE = 'uniform_republic/advisor/delete';
    
    /** Url path */
    const ADVISOR_URL_PATH_OPTION = 'uniform_republic/sizechartvalue/index';
    
    const ADVISOR_URL_PATH_NEW_OPTION = 'uniform_republic/sizechartvalue/newAction';
   
    /** @var UrlInterface */
    protected $urlBuilder;

    /**
     * @var string
     */
    private $editUrl;
    
    /**
     *
     * @var string
     */
    private $deleteUrl;
    
    /**
     *
     * @var string
     */
    private $optionUrl;
    
    /**
     *
     * @var string
     */
    private $newOptionUrl;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     * @param string $editUrl
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = [],
        $editUrl = self::ADVISOR_URL_PATH_EDIT,
        $deleteUrl = self::ADVISOR_URL_PATH_DELETE,
        $optionUrl = self::ADVISOR_URL_PATH_OPTION,
        $newOptionUrl = self::ADVISOR_URL_PATH_NEW_OPTION
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->editUrl = $editUrl;
        $this->deleteUrl = $deleteUrl;
        $this->optionUrl = $optionUrl;
        $this->newOptionUrl = $newOptionUrl;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                if (isset($item['sizechart_id'])) {
                    $item[$name]['edit'] = [
                        'href' => $this->urlBuilder->getUrl($this->editUrl, ['sizechart_id' => $item['sizechart_id']]),
                        'label' => __('Edit')
                    ];
                    
                    $item[$name]['delete'] = [
                        'href' => $this->urlBuilder->getUrl($this->deleteUrl, ['sizechart_id' => $item['sizechart_id']]),
                        'label' => __('Delete'),
                        'confirm' => [
                            'title' => __('Delete "${ $.$data.name }"'),
                            'message' => __('Are you sure you wan\'t to delete a "${ $.$data.name }" record?')
                        ]
                    ];
                    
                    $item[$name]['view'] = [
                        'href' => $this->urlBuilder->getUrl($this->optionUrl, ['sizechart_id' => $item['sizechart_id']]),
                        'label' => __('View Options')
                    ];
                    
                    $item[$name]['new'] = [
                        'href' => $this->urlBuilder->getUrl($this->newOptionUrl, ['sizechart_id' => $item['sizechart_id']]),
                        'label' => __('Add New Options')
                    ];
                }
            }
        }

        return $dataSource;
    }
}
