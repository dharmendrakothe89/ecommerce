<?php

namespace Uniform\Republic\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if (!$installer->tableExists( 'republic_sizechart' )) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable( 'republic_sizechart' )
            )
                ->addColumn(
                    'sizechart_id', Table::TYPE_INTEGER, null, [
                    'identity' => true,
                    'nullable' => false,
                    'primary' => true,
                    'unsigned' => true,
                ], 'Chart size ID'
                )
                ->addColumn(
                    'name', Table::TYPE_TEXT, 255, ['nullable' => false], 'Size chart name'
                )
                ->addColumn(
                    'option_name', Table::TYPE_TEXT, 255, ['nullable' => true], 'Size chart option name'
                )
                ->addColumn(
                    'created_at', Table::TYPE_TIMESTAMP, null, ['nullable' => true, 'default' => Table::TIMESTAMP_INIT], 'Created At'
                )->addColumn(
                    'updated_at', Table::TYPE_TIMESTAMP, null, ['nullable' => true, 'default' => Table::TIMESTAMP_INIT_UPDATE], 'Updated At' )
                ->setComment( 'Size chart Table' );

            $installer->getConnection()->createTable( $table );
        }

        if (!$installer->tableExists( 'republic_sizechart_to_product' )) {
            $tableProduct = $installer->getConnection()->newTable(
                $installer->getTable( 'republic_sizechart_to_product' )
            )
                ->addColumn(
                    'sizechart_to_product_id', Table::TYPE_INTEGER, null, [
                    'identity' => true,
                    'nullable' => false,
                    'primary' => true,
                    'unsigned' => true,
                ], 'Sizechart to product ID'
                )
                ->addColumn(
                    'sizechart_id', Table::TYPE_INTEGER, null, ['nullable' => false], 'Size chart Id'
                )
                ->addColumn(
                    'product_id', Table::TYPE_INTEGER, null, ['nullable' => false], 'Product Id'
                )
                ->addColumn(
                    'image_advisor', Table::TYPE_TEXT, 255, ['nullable' => true], 'Advisor image'
                )
                ->addColumn(
                    'created_at', Table::TYPE_TIMESTAMP, null, ['nullable' => true, 'default' => Table::TIMESTAMP_INIT], 'Created At'
                )->addColumn(
                    'updated_at', Table::TYPE_TIMESTAMP, null, ['nullable' => true, 'default' => Table::TIMESTAMP_INIT_UPDATE], 'Updated At' )
                ->setComment( 'Sizechart to product Table' );

            $installer->getConnection()->createTable( $tableProduct );
        }

        if (!$installer->tableExists( 'republic_sizechart_value' )) {
            $tableValue = $installer->getConnection()->newTable(
                $installer->getTable( 'republic_sizechart_value' )
            )
                ->addColumn(
                    'value_id', Table::TYPE_INTEGER, null, [
                    'identity' => true,
                    'nullable' => false,
                    'primary' => true,
                    'unsigned' => true,
                ], 'Sizechart value ID'
                )
                ->addColumn(
                    'sizechart_id', Table::TYPE_INTEGER, null, ['nullable' => false], 'Size chart Id'
                )
                ->addColumn(
                    'value1', Table::TYPE_TEXT, 255, ['nullable' => true], 'Value 1'
                )
                ->addColumn(
                    'value2', Table::TYPE_TEXT, 255, ['nullable' => true], 'Value 2'
                )
                ->addColumn(
                    'created_at', Table::TYPE_TIMESTAMP, null, ['nullable' => true, 'default' => Table::TIMESTAMP_INIT], 'Created At'
                )->addColumn(
                    'updated_at', Table::TYPE_TIMESTAMP, null, ['nullable' => true, 'default' => Table::TIMESTAMP_INIT_UPDATE], 'Updated At' )
                ->setComment( 'Sizechart to product Table' );

            $installer->getConnection()->createTable( $tableValue );
        }

        $installer->endSetup();
    }

}
