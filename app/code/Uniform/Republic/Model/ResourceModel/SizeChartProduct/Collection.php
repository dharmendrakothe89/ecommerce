<?php
namespace Uniform\Republic\Model\ResourceModel\SizeChartProduct;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'sizechart_to_product_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Uniform\Republic\Model\SizeChartProduct', 'Uniform\Republic\Model\ResourceModel\SizeChartProduct');
    }

}
