<?php

namespace Uniform\Republic\Controller\Adminhtml\Advisor;

use Magento\Framework\Controller\ResultFactory;

class Delete extends \Magento\Backend\App\Action {

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute() {
        $id = $this->getRequest()->getParam('sizechart_id');
        $model = $this->_objectManager->create('Uniform\Republic\Model\SizeChart')->load($id);

        if (!$model->getId()) {
            $this->messageManager->addError(__('This data is no longer exists.'));
            /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();

            return $resultRedirect->setPath('*/*/');
        }

        $advisorProduct = $this->_objectManager->create('Uniform\Republic\Model\SizeChartProduct')->load($model->getId(), 'sizechart_id');
        if (!$advisorProduct->getId()) {
            $model->delete();

            $this->messageManager->addSuccess(
                    __('A size advisor has been deleted.')
            );
        } else {
            $this->messageManager->addError(
                    __('A size advisor has been assigned to product so can not detele.')
            );
        }

        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('*/*/index');
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed() {
        return $this->_authorization->isAllowed('Uniform_Republic::advisors');
    }

}
