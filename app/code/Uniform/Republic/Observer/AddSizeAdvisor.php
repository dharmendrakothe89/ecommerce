<?php

namespace Uniform\Republic\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Filesystem;

class AddSizeAdvisor implements ObserverInterface {

    /**
     *
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     *
     * @var Filesystem 
     */
    protected $_fileSystem;

    /**
     * 
     * @param ObjectManagerInterface $objectManager
     * @param Filesystem $fileSystem
     */
    public function __construct(ObjectManagerInterface $objectManager, Filesystem $fileSystem) {
        $this->_objectManager = $objectManager;
        $this->_fileSystem = $fileSystem;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        $product = $observer->getProduct();
        $sizeChartModel = $this->_objectManager->create('Uniform\Republic\Model\SizeChartProduct')->load($product->getId(), 'product_id');

        if (isset($_POST['product']['size_advisor_hidden']) && !empty($_POST['product']['size_advisor_hidden'])) {
            if (isset($_POST['product']['size_advisor_image_remove']) && $_POST['product']['size_advisor_image_remove'] == 1) {
                $mediaDirectory = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
                $path = $mediaDirectory->getAbsolutePath('size_advisor');
                $target = $path . '/' . $sizeChartModel->getImageAdvisor();
                if (!empty($sizeChartModel->getImageAdvisor()) && file_exists($target)) {
                    unlink($target);
                }

                $sizeChartModel->setImageAdvisor(null);
            }
            $sizeChartModel->setProductId($product->getId());
            $sizeChartModel->setSizechartId($_POST['product']['size_advisor_hidden']);
            $result = $this->_uploadFile();
            if (is_array($result) && isset($result['file'])) {
                if (!empty($sizeChartModel->getImageAdvisor())) {
                    $mediaDirectory = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
                    $path = $mediaDirectory->getAbsolutePath('size_advisor');
                    $target = $path . '/' . $sizeChartModel->getImageAdvisor();
                    if (!empty($sizeChartModel->getImageAdvisor()) && file_exists($target)) {
                        unlink($target);
                    }
                }
                $sizeChartModel->setImageAdvisor($result['file']);
            }
            $sizeChartModel->save();
        } else {
            if ($sizeChartModel->getId()) {

                $mediaDirectory = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
                $path = $mediaDirectory->getAbsolutePath('size_advisor');
                $target = $path . '/' . $sizeChartModel->getImageAdvisor();
                if (!empty($sizeChartModel->getImageAdvisor()) && file_exists($target)) {
                    unlink($target);
                }

                $sizeChartModel->delete();
            }
        }

        $sizeChartModel = $this->_objectManager->create('Uniform\Republic\Model\SizeChartProduct')->load($product->getId(), 'product_id');
        if (isset($_POST['product']['size_chart_image_remove']) && $_POST['product']['size_chart_image_remove'] == 1) {
            $mediaDirectory = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
            $path = $mediaDirectory->getAbsolutePath('size_chart');
            $target = $path . '/' . $sizeChartModel->getSizechartImage();
            if (!empty($sizeChartModel->getSizechartImage()) && file_exists($target)) {
                unlink($target);
            }

            $sizeChartModel->setSizechartImage(null);
        }
        $sizeChartImage = $this->_uploadChartFile();
        if (is_array($sizeChartImage) && isset($sizeChartImage['file'])) {
            if (!empty($sizeChartModel->getSizechartImage())) {
                $mediaDirectory = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
                $path = $mediaDirectory->getAbsolutePath('size_chart');
                $target = $path . '/' . $sizeChartModel->getSizechartImage();
                if (file_exists($target)) {
                    unlink($target);
                }
            }
            $sizeChartModel->setSizechartImage($sizeChartImage['file']);
        }
        
        $sizeChartModel->save();
    }

    protected function _uploadFile() {
        try {
            if ($_FILES['product']['name']['size_advisor_image']) {
                $uploader = $this->_objectManager->create(
                        'Magento\MediaStorage\Model\File\Uploader', ['fileId' => 'product[size_advisor_image]']
                );
                $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                $uploader->setAllowRenameFiles(true);
                $mediaDirectory = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
                $path = $mediaDirectory->getAbsolutePath('size_advisor');
                $result = $uploader->save($path);
                return $result;
            }

            return null;
        } catch (\Exception $e) {
            return null;
        }
    }

    protected function _uploadChartFile() {
        try {
            if ($_FILES['product']['name']['size_chart_image']) {
                $uploader = $this->_objectManager->create(
                        'Magento\MediaStorage\Model\File\Uploader', ['fileId' => 'product[size_chart_image]']
                );
                $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                $uploader->setAllowRenameFiles(true);
                $mediaDirectory = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
                $path = $mediaDirectory->getAbsolutePath('size_chart');
                $result = $uploader->save($path);
                return $result;
            }

            return null;
        } catch (\Exception $e) {
            return null;
        }
    }

}
