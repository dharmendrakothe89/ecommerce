<?php

namespace Uniform\Republic\Api\Data;

interface SizeChartInterface {

    /**
     * Constants for keys of data array.
     */
    const SIZE_CHART_ID = 'sizechart_id';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

     /**
     * Set ID
     *
     * @param int $id
     * @return SizeChartInterface
     */
    public function setId($id);
}