<?php
namespace Uniform\Republic\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;

class SizeChartValueActions extends Column
{
    /** Url path */
    const ADVISOR_URL_PATH_EDIT = 'uniform_republic/sizechartvalue/edit';
    
    /** Url path */
    const ADVISOR_URL_PATH_DELETE = 'uniform_republic/sizechartvalue/delete';
   
    /** @var UrlInterface */
    protected $urlBuilder;

    /**
     * @var string
     */
    private $editUrl;
    
    /**
     *
     * @var string
     */
    private $deleteUrl;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     * @param string $editUrl
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = [],
        $editUrl = self::ADVISOR_URL_PATH_EDIT,
        $deleteUrl = self::ADVISOR_URL_PATH_DELETE
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->editUrl = $editUrl;
        $this->deleteUrl = $deleteUrl;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                if (isset($item['sizechart_id'])) {
                    $item[$name]['edit'] = [
                        'href' => $this->urlBuilder->getUrl($this->editUrl, ['value_id' => $item['value_id']]),
                        'label' => __('Edit')
                    ];
                    
                    $item[$name]['delete'] = [
                        'href' => $this->urlBuilder->getUrl($this->deleteUrl, ['value_id' => $item['value_id']]),
                        'label' => __('Delete'),
                        'confirm' => [
                            'title' => __('Delete "${ $.$data.value1 }"'),
                            'message' => __('Are you sure you wan\'t to delete a "${ $.$data.value1 }" record?')
                        ]
                    ];
                }
            }
        }

        return $dataSource;
    }
}