<?php
namespace Uniform\Republic\Block\Adminhtml\SizeChartValue\Edit;

/**
 * Adminhtml size chart value form
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{

    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('sizechartvalue_form');
        $this->setTitle(__('Size Chart Value Information'));
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \Uniform\Republic\Model\SizeChart $model */
        $model = $this->_coreRegistry->registry('sizechart_value');

        $url = $this->_urlBuilder->getUrl('uniform_republic/sizechartvalue/save', ['sizechart_id' => $model->getSizeChartId()]);
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $url, 'method' => 'post']]
        );

       // $form->setHtmlIdPrefix('sizechart_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('Size Chart Value Information'), 'class' => 'fieldset-wide']
        );

        if ($model->getId()) {
            $fieldset->addField('value_id', 'hidden', ['name' => 'value_id']);
        }

        $fieldset->addField('sizechart_id', 'hidden', ['name' => 'sizechart_id', 'value' => 3]);
        
        $fieldset->addField(
            'value1',
            'text',
            ['name' => 'value1', 'label' => __('Option'), 'title' => __('Option'), 'required' => true]
        );

        /*$fieldset->addField(
            'value2',
            'text',
            ['name' => 'value2', 'label' => __('Option Value'), 'title' => __('Option Value'), 'required' => true]
        );*/
        
        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}