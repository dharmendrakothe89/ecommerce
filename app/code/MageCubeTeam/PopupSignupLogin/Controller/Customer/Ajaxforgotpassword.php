<?php

/**
 * MageCubeTeam
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageCubeTeam.com license that is
 * available through the world-wide-web at this URL:
 * https://www.magecube.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageCubeTeam
 * @package     MageCubeTeam_PopupSignupLogin
 * @copyright   Copyright (c) 2018 MageCubeTeam (http://www.magecube.com/)
 * @license     https://www.magecube.com/LICENSE.txt
 */

namespace MageCubeTeam\PopupSignupLogin\Controller\Customer;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\AccountManagement;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Escaper;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\SecurityViolationException;

/**
 * ForgotPasswordPost controller
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Ajaxforgotpassword extends \Magento\Framework\App\Action\Action {

    /**
     * @var \Magento\Customer\Api\AccountManagementInterface
     */
    protected $customerAccountManagement;

    /**
     * @var \Magento\Framework\Escaper
     */
    protected $escaper;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @param Context $context
     * @param Session $customerSession
     * @param AccountManagementInterface $customerAccountManagement
     * @param Escaper $escaper
     */
    public function __construct(
    Context $context, Session $customerSession, AccountManagementInterface $customerAccountManagement, Escaper $escaper, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
    ) {
        parent::__construct($context);
        $this->session = $customerSession;
        $this->customerAccountManagement = $customerAccountManagement;
        $this->escaper = $escaper;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resultRawFactory = $resultRawFactory;
    }

    /**
     * Forgot customer password action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute() {

        $userData = null;
        $httpBadRequestCode = 400;
        $credentials = $this->getRequest()->getParams();
        $response = [
            'errors' => false,
            'message' => __('Please correct the email address.')
        ];

        /** @var \Magento\Framework\Controller\Result\Raw $resultRaw */
        $resultRaw = $this->resultRawFactory->create();
        try {
            $userData = $credentials;
        } catch (\Exception $e) {
            return $resultRaw->setHttpResponseCode($httpBadRequestCode);
        }
        if (!$userData || $this->getRequest()->getMethod() !== 'POST' || !$this->getRequest()->isXmlHttpRequest()) {
            return $resultRaw->setHttpResponseCode($httpBadRequestCode);
        }

        $email = (string) $this->getRequest()->getPost('email');

        if ($email) {
            if (!\Zend_Validate::is($email, \Magento\Framework\Validator\EmailAddress::class)) {
                $this->session->setForgottenEmail($email);

                $response = [
                    'errors' => true,
                    'message' => __('Please correct the email address.')
                ];
            }

            try {
                $this->customerAccountManagement->initiatePasswordReset(
                        $email, AccountManagement::EMAIL_RESET
                );
                $response = [
                    'errors' => false,
                    'message' => $this->getSuccessMessage($email)
                ];
            } catch (NoSuchEntityException $exception) {
                // Do nothing, we don't want anyone to use this action to determine which email accounts are registered.
                $response = [
                    'errors' => true,
                    'message' => __($exception->getMessage())
                ];
            } catch (SecurityViolationException $exception) {
                $response = [
                    'errors' => true,
                    'message' => __($exception->getMessage())
                ];
            } catch (\Exception $exception) {
                $response = [
                    'errors' => true,
                    'message' => __('We\'re unable to send the password reset email.')
                ];
            }
        } else {
            $response = [
                'errors' => true,
                'message' => __('Please enter your email.')
            ];
        }

        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($response);
    }

    /**
     * Retrieve success message
     *
     * @param string $email
     * @return \Magento\Framework\Phrase
     */
    protected function getSuccessMessage($email) {
        return __(
                'If there is an account associated with %1 you will receive an email with a link to reset your password.', $this->escaper->escapeHtml($email)
        );
    }

}
