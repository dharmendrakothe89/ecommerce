<?php
namespace Uniform\Republic\Controller\Adminhtml\Advisor;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Uniform_Republic::advisors');
        $resultPage->addBreadcrumb(__('Size Advisors'), __('Size Advisors'));
        $resultPage->addBreadcrumb(__('Manage Size Advisors'), __('Manage Size Advisors'));
        $resultPage->getConfig()->getTitle()->prepend(__('Size Advisors'));

        return $resultPage;
    }

    /**
     * Is the user allowed to view the advisors
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Uniform_Republic::advisors');
    }


}
